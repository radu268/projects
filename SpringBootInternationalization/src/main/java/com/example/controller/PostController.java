package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by inrad on 10/9/2016.
 */
@Controller
@RequestMapping("/posts")
public class PostController {

    @RequestMapping("/")
    public String list(){
        return "views/list";
    }

}