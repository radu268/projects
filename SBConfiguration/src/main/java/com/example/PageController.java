package com.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by inrad on 10/6/2016.
 */
@RestController
public class PageController {

    @Value("${pageController.message}")
    private String pageControllerMessage;
    @Value("${app.version}")
    private String appVersion;

    @RequestMapping("/")
    public String page() {
        return pageControllerMessage;
    }

    @RequestMapping("/pages")
    public String pages() {
        return appVersion;
    }
}
