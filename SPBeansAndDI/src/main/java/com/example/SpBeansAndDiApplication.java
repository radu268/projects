package com.example;

import abc.foo.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@ComponentScan({"com.example", "abc.foo"})
@SpringBootApplication  //= @Configuration + @EnableAutoCOnfiguration + @ComponentScan
public class SpBeansAndDiApplication {

    @Bean
    public User user() {
        return new User("dan", "vegan");
    }

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(SpBeansAndDiApplication.class, args);
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String name : beanNames) {
            System.out.println(name);
        }


        System.out.println(ctx.getBean("user").toString());

    }
}
