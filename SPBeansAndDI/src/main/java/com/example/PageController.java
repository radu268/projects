package com.example;

import abc.foo.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by inrad on 10/6/2016.
 */
@RestController
public class PageController {


 /*   Property */
//    @Autowired
//    private NotificationService notificationService;

 /*   setter */
//    private NotificationService notificationService;
//    @Autowired
//    public void setNotificationService(NotificationService notificationService) {
//        this.notificationService = notificationService;
//    }

    /*   Constructor */
    private NotificationService notificationService;

    @Autowired
    public PageController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RequestMapping("/page")
    public String getPage() {

        return notificationService.toString();
    }

}
