package abc.foo;

import org.springframework.stereotype.Service;

/**
 * Created by inrad on 10/6/2016.
 */
@Service
//@Service("notificationService")
public class NotificationService {

    public NotificationService() {
    }

    public void send() {

    }

    public void sendAsync() {
    }

    @Override
    public String toString() {
        return "NotificationService";
    }
}
