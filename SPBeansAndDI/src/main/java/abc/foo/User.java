package abc.foo;

/**
 * Created by inrad on 10/6/2016.
 */
public class User {

    private String firstName;
    private String laseName;
    private String emailAddress;

    public User(String firstName, String laseName) {
        this.firstName = firstName;
        this.laseName = laseName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLaseName() {
        return laseName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLaseName(String laseName) {
        this.laseName = laseName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", laseName='" + laseName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
