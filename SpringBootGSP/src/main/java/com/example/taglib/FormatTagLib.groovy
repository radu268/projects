package com.example.taglib

import grails.gsp.TagLib
import org.springframework.stereotype.Component

import java.text.SimpleDateFormat

/**
 * Created by inrad on 10/9/2016.
 */
@Component
@TagLib
class FormatTagLib {
    def dateFormat = { attrs, body ->
        out << new SimpleDateFormat(attrs.format).format(attrs.date)
    }
}
