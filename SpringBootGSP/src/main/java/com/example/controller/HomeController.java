package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by inrad on 10/9/2016.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String home(Model model){
        model.addAttribute("name","Dan");
        return "index";
    }

}