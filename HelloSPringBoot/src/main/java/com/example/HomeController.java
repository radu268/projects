package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by inrad on 10/6/2016.
 */
@RestController
public class HomeController {

    @RequestMapping("/")
    public String home(){
        return "SpringBoot";
    }

}
