package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SbProfilesApplication {

	public static void main(String[] args) {

	ApplicationContext ctx= SpringApplication.run(SbProfilesApplication.class, args);
		System.out.println(ctx.getBean("dataSource").toString());
	}
}
