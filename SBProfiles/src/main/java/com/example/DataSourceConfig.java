package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by inrad on 10/6/2016.
 */
@Configuration
public class DataSourceConfig {

    @Bean(name = "dataSource")
    @Profile("development")
    DataSource development() {
        return new DataSource("dev-url",9999);
    }

    @Bean
    @Profile("dataSource")
    DataSource production() {
        return new DataSource("dev-url",9999);
    }

}
