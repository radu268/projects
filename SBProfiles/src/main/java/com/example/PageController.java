package com.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by inrad on 10/6/2016.
 */
@RestController
public class PageController {

    @Value("${spring.profiles.active}")
    private String environment;
    @Value("${msg}")
    private String msg;

    @RequestMapping("/")
    public String page() {
        return msg;
    }
}
