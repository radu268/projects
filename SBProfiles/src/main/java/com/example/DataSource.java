package com.example;

/**
 * Created by inrad on 10/6/2016.
 */
public class DataSource {

    private String server;
    private int port;

    public DataSource() {
    }

    public DataSource(String server, int port) {
        this.server = server;
        this.port = port;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getServer() {

        return server;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "DataSource{" +
                "server='" + server + '\'' +
                ", port=" + port +
                '}';
    }
}
